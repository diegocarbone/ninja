$(document).ready( function () {

	$('#boton-ingreso').click( function (e) {
		e.preventDefault();

		if ($('#login-container').hasClass('open')) {
			$('#login-container').removeClass('open');
			$('#boton-ingreso').removeClass('open');
		}
		else {
			$('#login-container').addClass('open');
			$('#boton-ingreso').addClass('open');
		}

	});

	$('.toggle-password').click(function () {
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
			input.attr("type", "text");
			$(this).toggleClass("icon-coronita_visual icon-coronita_hide");
        } else {
			input.attr("type", "password");
			$(this).toggleClass("icon-coronita_hide icon-coronita_visual");
        }
	});
	

	$( "input" ).change(function() {
		var valor = $(this).val();
		if (valor != "") {
			$(this).addClass("has-text");
		} else {
			$(this).removeClass("has-text");
		}
	  });
})