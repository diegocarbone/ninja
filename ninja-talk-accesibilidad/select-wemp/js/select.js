var x, i, j, selElmnt, a, b, c;

// Le pongo tabindex=1 a todos los campos para que el flow sea en el orden correcto
var tabs = document.querySelectorAll("input, select");
for (i = 0; i < tabs.length; i++) {
    tabs[i].setAttribute("tabindex","1");
}

/* Busco todos los custom-selects de la pagina */
x = document.getElementsByClassName("custom-select");

for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];

    /* Creo el elemento seleccionado */
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.setAttribute("tabindex", "1");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);

    /* Creo la lista de opciones */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");

    for (j = 1; j < selElmnt.length; j++) {

        /* Creo las opciones del select */
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;

        c.setAttribute("tabindex", "1");

        c.addEventListener("keyup", function(e) {
            var direccion = 0;
            switch (e.keyCode) {
                case 13: { /* Enter */
                    seleccionarItem (this);
                    e.target.parentNode.previousSibling.focus();
                    break;
                }
                case 38: { /* Up */
                    var seleccionado = this.parentNode.querySelector('div.same-as-selected');
                    if (seleccionado) { seleccionado.classList.remove('same-as-selected'); }
                    var sibling = encontrarSibling (this, direccion = -1, e);
                    sibling.focus();
                    break;
                }
                case 40: { /* Down */
                    var seleccionado = this.parentNode.querySelector('div.same-as-selected');
                    if (seleccionado) { seleccionado.classList.remove('same-as-selected'); }
                    var sibling = encontrarSibling (this, direccion = 1, e);
                    sibling.focus();
                    break;
                }
            }
        });

        c.addEventListener("click", function(e) {
            seleccionarItem (this);
            e.target.parentNode.previousSibling.focus();
        });
        
        b.appendChild(c);
        
    }

    x[i].appendChild(b);

    a.addEventListener("click", function(e) {
        toggleOpciones(e, this);
    });
    
    // Enter - Space - Down Key
    a.addEventListener("keyup", function(e) {
        if (event.keyCode === 32 || event.keyCode === 13 || event.keyCode === 40) { // 32 Space // 13 Enter // 40 Down
            toggleOpciones(e, this);
        }
    });

    // Si hay un TAB dentro de select-items, y no es en el input, redirijo el foco al input
    b.addEventListener("keydown", function (e) {
        if (e.keyCode == 9) {
            if (this.parentNode.classList.contains('live-search')) {
                if (e.target.tagName.toLowerCase() != "input") {
                    this.querySelector('input').focus();
                }
                else {
                    var seleccionado = this.querySelector('.same-as-selected');
                    if (seleccionado) { seleccionado.classList.remove('same-as-selected'); }
                }
            }   
        }
    });

}

function seleccionarItem (el) {
    /* actualizo el item seleccionado en el select original y en select-selected */
    var y, i, k, s, h;
    s = el.parentNode.parentNode.getElementsByTagName("select")[0];
    h = el.parentNode.previousSibling;
    
    for (i = 0; i < s.length; i++) {
        if (s.options[i].innerHTML == el.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = el.innerHTML;
            y = el.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
            }
            el.setAttribute("class", "same-as-selected");
            break;
        }
    }
    h.click();
}

function enfocarSeleccionado (e) {
    var items = e.target;
    var select = items.closest('.custom-select').querySelector('select');
    // Uso selectedIndex-1 porque en la lista select-items no está el item con value="0"
    var seleccionado = items.getElementsByTagName('div')[select.selectedIndex-1];
    
    if (seleccionado) { 
        seleccionado.focus(); 
        seleccionado.classList.remove('same-as-selected');
    }
    else { 
        items.firstChild.focus();
    }
}

function toggleOpciones (e, elem) {
    e.stopPropagation();
    closeAllSelect(elem);
    elem.classList.toggle("select-arrow-active");
    var selectItems = elem.nextSibling;
    selectItems.classList.toggle("select-hide");

    // Si no es un live-search, pongo el foco en el item seleccionado, si no hay ninguno el foco va al primer item
    if ( Array.from(selectItems.parentNode.classList).indexOf('live-search') < 0 ) {
        selectItems.removeEventListener ("transitionend", enfocarSeleccionado );
        selectItems.addEventListener ("transitionend", enfocarSeleccionado );
    }
    else {
        // Al abrir o cerrar el select, vacio el input
        selectItems.getElementsByTagName('input')[0].value = "";
        var opciones = Array.from(document.getElementsByClassName('oculto'));
        for (var i=0; i < opciones.length; i++) {
            opciones[i].classList.remove("oculto");
        }
    }
}

function closeAllSelect(elmnt) {
    /* Cierra todos los selects menos el actual */
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

function encontrarSibling(seleccionado, direccion, e) {
    var lista = Array.from(seleccionado.parentNode.querySelectorAll('div:not(.oculto)'));
    var pos = lista.indexOf(seleccionado);

    if (direccion < 0) {
        // La dirección es hacia arriba, y no intenta ir mas alla de la primera posicion
        if (pos > 0) {
            return lista[pos-1];
        }
    }
    else {
        // La dirección es hacia abajo, y no intenta ir mas alla del tamaño de la lista
        if (pos < lista.length - 1) {
            return lista[pos+1];
        }
    }

    // Si el elemento es border, no se mueve, devuelve el mismo elemento
    return lista[pos];
}

/* Cierrar todos los selects si se clickea fuera de ellos */
document.addEventListener("click", closeAllSelect);

// Cierro el select cuando hay un ESC y le devuelvo el foco al select
document.addEventListener("keyup", function(event) {
    if (event.keyCode === 27) {
        // Si el ESC se produce dentro de un custom-select vuelvo a marcar el item seleccionado y le devuelvo el foco al select
        var customSelect = event.target.closest(".custom-select");
        if (customSelect) {
            var seleccionado = customSelect.getElementsByTagName("select")[0].selectedIndex;
            if (seleccionado) {
                var items = customSelect.querySelector('.select-items').getElementsByTagName('div');
                items[seleccionado - 1].classList.add('same-as-selected');
            }

            customSelect.querySelector('.select-selected').focus();
        }
        closeAllSelect(this);
    }
});


window.onkeydown = function(e) { 
  
    // Si estoy dentro del input habilito todas las teclas
    if (e.target.tagName.toLowerCase() == "input") {
        return e.keyCode;
    }
    
    if (Array.from(e.target.parentNode.classList).indexOf('select-items') > -1) {
        return !(e.keyCode == 32) && !(e.keyCode == 38) && !(e.keyCode == 40) && !(e.keyCode == 9);
    }

    // Si uso las flechas dentro de un select, no scrollea la página
    if (Array.from(e.target.classList).indexOf('select-selected') > -1) {
        return !(e.keyCode == 32) && !(e.keyCode == 38) && !(e.keyCode == 40);
    }
    
    // Evito que Space scrollee
    return !(e.keyCode == 32);
};