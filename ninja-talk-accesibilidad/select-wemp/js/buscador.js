var liveSearch, i, j, busqueda, busquedaForm, opciones;

liveSearch = document.getElementsByClassName("live-search");

for (i = 0; i < liveSearch.length; i++) {
  
    selectItems = liveSearch[i].getElementsByClassName("select-items")[0];
    
    
    // Creo el campo de busqueda en un formulario separado, para que su valor no se envie al servidor
    busqueda = document.createElement("input");
    busqueda.setAttribute("tabindex", "1");
    busqueda.setAttribute("type", "text");
    busqueda.setAttribute("name", "busqueda");
    busqueda.setAttribute("autocomplete", "off");
    busqueda.setAttribute("placeholder", "podés filtrar la lista...");
    busquedaForm = document.createElement("form");
    busquedaForm.appendChild(busqueda);
    selectItems.insertBefore(busquedaForm, selectItems.firstChild);

    // Evita que se cierre el select al hacer click en el input
    busqueda.addEventListener("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
    });
    
    var selectS = liveSearch[i].getElementsByClassName('select-selected')[0];

    selectS.addEventListener("transitionend", function(e) {
        var campo = this.nextSibling.getElementsByTagName("input")[0];
        campo.focus();
    });

    busqueda.parentNode.addEventListener("submit", function(e) {
        e.preventDefault();
    });

    busqueda.addEventListener("focus", function() {
        selectItems = this.parentNode.parentNode;
        opciones = Array.from(selectItems.getElementsByTagName('div')).map( function (e) { return e.innerHTML; } );
    });
    
    busqueda.addEventListener("input", function() {
        filtrar(this.value, opciones);
    });

}

function filtrar(input, ops) {
    
    var opsFiltradas = ops.filter( el => el.toLowerCase().indexOf(input.toLowerCase()) > -1 ).map( el => el.toLowerCase() );
    var divOptions = selectItems.getElementsByTagName('div');
    
    for (j = 0; j < divOptions.length; j++) {
        
        // Oculto las opciones que no coinciden con la busqueda, si el campo esta vacio vuelvo a mostrar todas las opciones
        if ( opsFiltradas.indexOf( divOptions[j].innerHTML.toLowerCase() ) > -1 || !input.length ) {
            divOptions[j].classList.remove("oculto");
        }
        else {
            divOptions[j].classList.add("oculto");
        }
    }
}